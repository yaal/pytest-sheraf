import relstorage
import ZEO.ClientStorage
import ZODB

import sheraf


def test_database(sheraf_database):
    assert isinstance(sheraf_database, sheraf.databases.Database)
    with sheraf.connection(commit=True) as conn:
        conn.root()["foobar"] = True
    assert isinstance(sheraf_database.storage, ZODB.DemoStorage.DemoStorage)


def test_filestorage_database(sheraf_filestorage_database):
    assert isinstance(sheraf_filestorage_database, sheraf.databases.Database)
    with sheraf.connection(commit=True) as conn:
        conn.root()["foobar"] = True
    assert isinstance(sheraf_filestorage_database.storage, ZODB.FileStorage.FileStorage)


def test_relstorage_database(sheraf_pgsql_relstorage_database):
    assert isinstance(sheraf_pgsql_relstorage_database, sheraf.databases.Database)
    with sheraf.connection(commit=True) as conn:
        conn.root()["foobar"] = True
    assert isinstance(
        sheraf_pgsql_relstorage_database.storage, relstorage.storage.RelStorage
    )


def test_connection(sheraf_connection):
    assert isinstance(sheraf_connection, ZODB.Connection.Connection)
    sheraf.Database.current_connection().root()["foobar"] = True
    sheraf.Database.current_connection().transaction_manager.commit()


class TestCustomDatabaseParameters:
    sheraf_database_kwargs = {"nestable": True}

    def test_custom_database_paremeters(self, sheraf_database):
        assert sheraf_database.nestable


class TestCustomExtraDatabaseParameters:
    sheraf_database_kwargs = {"nestable": True}
    sheraf_database_extra_kwargs = {"nestable": False}

    def test_custom_database_paremeters(self, sheraf_database):
        assert not sheraf_database.nestable


def test_zeo_database(sheraf_zeo_database):
    assert isinstance(sheraf_zeo_database, sheraf.databases.Database)
    assert isinstance(sheraf_zeo_database.storage, ZEO.ClientStorage.ClientStorage)
    with sheraf.connection(commit=True) as conn:
        conn.root()["foobar"] = True
